import React from 'react';
import PropTypes from 'prop-types';

import { Question } from 'components';

class Questions extends React.Component {
    render() {
        const { questions, changeQuestion, removeQuestion } = this.props;

        return (
            questions.map(q => (
                <Question
                    key={q.id}
                    question={q}
                    changeQuestion={changeQuestion}
                    removeQuestion={removeQuestion}
                />
            ))
        );
    }
}

Questions.propTypes = {
    questions: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            type: PropTypes.string.isRequired,
        }),
    ).isRequired,
    changeQuestion: PropTypes.func.isRequired,
    removeQuestion: PropTypes.func.isRequired,
};

export default Questions;
