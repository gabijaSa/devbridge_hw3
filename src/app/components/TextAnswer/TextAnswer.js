import React from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField/TextField';

const TextAnswer = ({ multiLine, onChange, value }) => (
    <TextField
        label="Your Answer"
        margin="normal"
        fullWidth
        multiline={multiLine}
        value={value}
        onChange={e => onChange(e.target.value)}
        type="text"
    />
);

TextAnswer.propTypes = {
    multiLine: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};

TextAnswer.defaultProps = {
    multiLine: false,
};

export default TextAnswer;
