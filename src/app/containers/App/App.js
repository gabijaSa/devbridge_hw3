import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { QuizGenerator, Home, Quiz } from 'containers';

import './app.scss';

export default () => (
    <div className="app">
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/generator" component={QuizGenerator} />
            <Route path="/quiz" component={Quiz} />
            <Redirect to="/" />
        </Switch>
    </div>
);
