import React from 'react';
import PropTypes from 'prop-types';
import connect from 'react-redux/es/connect/connect';

import { Header, QuizCodeForm } from 'components';
import { /* getQuiz as getQuizAction, */setQuiz } from 'state-management/actions/quiz';
import quiz from 'data/quiz';

const defaultQuiz = {
    id: 'some-other-id',
    code: 'default',
    title: 'Default quiz when code not right',
    description: 'hamlet',
    active: true,
    questions: [
        {
            id: 2,
            text: 'Why?',
            type: 'LONG',
            correctAnswers: ['yes', 'no', 'maybe'],
        },
    ],
};

const Home = props => (
    <React.Fragment>
        <Header title="Feedback Matters" />
        <QuizCodeForm
            isLoading={props.isLoading}
            error={props.error}
            submitForm={props.getQuiz}
            activeCode={props.quizCode}
        />
    </React.Fragment>
);

Home.propTypes = {
    isLoading: PropTypes.bool,
    error: PropTypes.string,
    getQuiz: PropTypes.func,
    quizCode: PropTypes.string,
};

Home.defaultProps = {
    isLoading: false,
    error: '',
    getQuiz: () => {},
    quizCode: '',
};

const mapStateToProps = state => ({
    isLoading: state.quiz.isLoading,
    error: state.quiz.error,
    quizCode: state.quiz.quiz.code,
});

const mapDispatchToProps = dispatch => ({
    // getQuiz: code => dispatch(getQuizAction(code)),

    getQuiz: (code) => {
        const newQuiz = quiz.code === code ? quiz : defaultQuiz;
        dispatch(setQuiz(newQuiz));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
