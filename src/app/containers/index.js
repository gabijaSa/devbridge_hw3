export { default as App } from 'containers/App/App';
export { default as Home } from 'containers/Home/Home';
export { default as Quiz } from 'containers/Quiz/Quiz';
export { default as QuizGenerator } from 'containers/QuizGenerator/QuizGenerator';
