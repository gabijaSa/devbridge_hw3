import '@babel/polyfill';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';

import blue from '@material-ui/core/colors/blue';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import createBrowserHistory from 'history/createBrowserHistory';

import store from 'state-management/store/storeFactory';
import { App } from 'containers';

const theme = createMuiTheme({
    palette: {
        primary: blue,
    },
    typography: {
        useNextVariants: true,
    },
});

render((
    <Provider store={store}>
        <ConnectedRouter history={createBrowserHistory()}>
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <App />
            </MuiThemeProvider>
        </ConnectedRouter>
    </Provider>
), document.getElementById('root'));
