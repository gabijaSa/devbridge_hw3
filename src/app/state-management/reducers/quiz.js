import {
    GET_QUIZ,
    GET_QUIZ_ERROR,
    GET_QUIZ_SUCCESS,
    SET_QUIZ,
} from 'state-management/constants/quiz';

const initialState = {
    isLoading: false,
    error: '',
    quiz: {
        id: 'some-refresh',
        code: 'initial',
        title: 'Initial quiz after refresh',
        description: 're',
        active: true,
        questions: [
            {
                id: 2,
                text: 'Why you refresh?',
                type: 'LONG',
                correctAnswers: ['yes', 'no', 'maybe'],
            },
        ],
    },
};

export default function quizReducer(state = initialState, action = {}) {
    switch (action.type) {
    case GET_QUIZ: return {
        ...state,
        isLoading: true,
        error: '',
    };
    case GET_QUIZ_SUCCESS: return {
        ...state,
        isLoading: false,
        error: '',
        quiz: action.quiz,
    };
    case GET_QUIZ_ERROR: return {
        ...state,
        isLoading: false,
        error: action.error,
    };
    case SET_QUIZ: return {
        ...state,
        quiz: action.quiz,
    };
    default:
        return state;
    }
}
