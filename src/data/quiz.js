const quiz = {
    id: 'some-id-123',
    code: 'VLN_4',
    title: 'Sample quiz',
    description: 'i have no idea what i am doing',
    active: true,
    questions: [
        {
            id: 1,
            text: 'How are you?',
            type: 'RADIO',
            answers: [
                {
                    id: 1,
                    text: 'yes',
                },
                {
                    id: 2,
                    text: 'left',
                },
                {
                    id: 3,
                    text: 'red',
                },
                {
                    id: 4,
                    text: '12',
                },
            ],
            correctAnswers: ['4'],
        },
        {
            id: 2,
            text: 'Why?',
            type: 'LONG',
            correctAnswers: ['yes', 'no', 'maybe'],
        },
        {
            id: 3,
            text: 'Favorite animal?',
            type: 'SHORT',
            correctAnswers: ['Honey badger'],
        },
        {
            id: 4,
            text: 'Which options you prefer',
            type: 'CHECK',
            answers: [
                {
                    id: 1,
                    text: 'Option 1',
                },
                {
                    id: 2,
                    text: 'Option 2',
                },
                {
                    id: 3,
                    text: 'Option 3',
                },
                {
                    id: 4,
                    text: 'Option 4',
                },
            ],
            correctAnswers: [
                'Option 4',
                'Option 2',
            ],
        },
    ],
};

export default quiz;
